<?php include('components/header.php'); ?>

<div style="margin-top:-100px;" class="container">
        <!-- TABS -->
        <div>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                    aria-controls="nav-home" aria-selected="true"><i class="fa fa-list"></i> Todas</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                    aria-controls="nav-profile" aria-selected="false"> <i class="fa fa-globe"></i> Sites
                    educacionais</a>
            </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div id="aulas">
                        <?php include('components/tableGen.php'); ?>
                    </div>
                </div>
                <div class="tab-pane fade text-left" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <br>
                    <a
                        href="https://www.google.com.br/maps/place/Brasil/@-10.6386507,-65.5812124,5937028m/data=!3m1!1e3!4m5!3m4!1s0x9c59c7ebcc28cf:0x295a1506f2293e63!8m2!3d-14.235004!4d-51.92528">
                        Google Maps</a><br>
                    <a href="https://www.smartkids.com.br/">SmartKids</a><br>
                    <a href="https://www.autodraw.com/">AutoDraw - Desenhos automáticos</a><br>
                    <a href="https://sketch.io/sketchpad/">SketchPad - Desenhos livres</a><br>
                </div>

            </div>
            <!-- TABS -->

    <?php include('components/modals.php'); ?>
    <?php include('components/footer.php'); ?>


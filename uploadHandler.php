<?php
    //error_reporting(0);
    require('components/config.php');
    include('components/header.php'); 
 ?>

<?php
    // Nas versões do PHP anteriores a 4.1.0, $HTTP_POST_FILES deve ser utilizado ao invés de $_FILES.
    //Dependendo do servidor é obvio que é necessário trocar o diretório dos arquivos.
    $uploaddir = $_SERVER['DOCUMENT_ROOT']."/aulasdigitais.dev/aulas/";

    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $aula = $_FILES['file']['name'][0];
    $dia = date("Y/m/d");
    $hora = date("h:i:sa");
    
    for($i=0;$i < count($_FILES['file']['name']);$i++){
    
        $uploadfile = $uploaddir.iconv("UTF-8", "ISO-8859-1//TRANSLIT", $_FILES['file']['name'][$i]);
        $movefile = $uploaddir.'zip/'.iconv("UTF-8", "ISO-8859-1//TRANSLIT", $_FILES['file']['name'][$i]);

        $ext = pathinfo($_FILES['file']['name'][$i], PATHINFO_EXTENSION);
        $allowed = ['zip'];

        // Se o upload estiver na extensão correta, executa o código abaixo
        if (in_array($ext, $allowed)) {
            move_uploaded_file($_FILES['file']['tmp_name'][$i], $movefile);
            
            echo $aula.'<br>';
            echo "<h4 class=text-success>Arquivo válido e enviado com sucesso.</h4><hr>";
         
            // Extraindo arquivo
            $zip = new ZipArchive();


            if($zip->open($movefile)  === true){
                $zip->extractTo($uploaddir);
                $zip->close();
                echo 'Sucesso ao extrair zip. <br>';
            }

            // Registrando no BD
            $sql = 'INSERT INTO aulas(nome,email,aula,dia,hora) VALUES( :nome, :email, :aula, :dia, :hora )';
            $query= $pdo->prepare($sql);
            $query->bindParam(':nome',$nome);
            $query->bindParam(':email',$email);
            $query->bindParam(':aula',$aula);
            $query->bindParam(':dia',$dia);
            $query->bindParam(':hora',$hora);
            $inserir = $query->execute();
        
            if(!$inserir){
                //echo 'Erro ao inserir dados no banco!<br>';
                
            }
            else{
                //echo $query->rowCount() . ' Linha inserida';
            }           

            echo "<a class=\"btn btn-outline-success m-4\" href=\"index.php\">VOLTAR</a>";
            
        }
        
        else {
            echo "<h4 class=text-danger> Erro de upload do arquivo! </h4>";
        }
    }

    // print_r($_FILES);
    // print "</pre>";

?>

<?php include('components/footer.php'); ?>
# aulasdigitais.dev
Este repositório é paralelo ao aulasdigitais.online. Não há validação nenhuma dos uploads, pois essa é uma versão para desenvolvimento
O repositório 'aulasdigitais.online' tem a versão que está no ar, hospedada no Github Pages. Uma versão estática, ja que o Github Pages não permite 
processar páginas dinâmicas. O repositório 'aulasdigitais.dev' é uma versão melhorada, feito em PHP, ainda em fase de testes.

Apesar de existir um banco de dados, ele não faz nenhum cadastro. ele apenas faz registros dos uploads. As aulas são listadas através de uma
função que lê o conteúdo do diretório 'aulas'.

### Como instalar:

1 - Coloque a pasta inteira do site (aulasdigitais.dev) na raíz do server.
Ao fazer isso, a URL da página inicial será: http://{seu_server_host}/aulasdigitais.dev/index.php

2 - Rode o script database-script.sql no seu banco.

3 - Abra o site e verifique se está tudo em ordem (conexão com banco de dados e etc).




### Regras para upload de aulas:

1 - Deve ser um arquivo .zip

2 - NENHUM arquivo do zip deve conter caracteres especiais

3 - Essa é a estrutura do .zip:

Aula.zip

.. Pasta com a aula

...... index.html


Na pasta aulas, existe um zip para testes de upload de aulas.



    <!-- FOOTER -->
    <footer class="text-center text-muted mt-5 mb-4">
            © 2019 - <span>Diego Lopes</span>
    </footer>
    <!-- FOOTER -->

    <!-- JavaScript e livrarias -->
    <script src="lib/jquery/jquery-3.2.1.js"></script>
    <script src="lib/bootstrap-4.0.0/js/popper.js"></script>
    <script src="lib/bootstrap-4.0.0/js/bootstrap.js"></script>
    <script src="js/scripts.js"></script>

    <!-- Chunk upload -->
    <!-- <script src="js/chunkUpload.ajax.js"></script> -->
    <script type="text/javascript" src="chunkUploadMd5.js"></script>
    <script type="text/javascript" src="chunkUpload.js"></script>

</body>

</html>

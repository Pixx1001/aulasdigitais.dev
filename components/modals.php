<!-- Modal sobre -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4>Sobre o site:</h4>
                <p>
                Nosso site, o <i>aulasdigitais.online</i> nada mais é do que um site de compartilhamento de conteúdos didáticos. Aqui você pode simplesmente
                fazer o upload das suas aulas dgitais no nosso servidor e ele ficará de livre acesso ao público para download
                ou visualização na nossa lista de aulas digitais.
                <hr>
                Se você quer contribuir e me ajudar a melhorar o site você pode mandar um e-mail 
                mandando uma ativade do EdiLIM ou Ardora, ou algo similiar para o email: <b>d.lopes.karagua@gmail.com</b>

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
            </div>
        </div>
    </div>

    <!-- Modal sobre -->

    <!-- Modal de ajuda -->
        <div class="modal fade" id="modalAjuda" tabindex="-1" role="dialog" aria-labelledby="modalAjudaLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalAjudaLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4>Ajuda:</h4>
                    <p>
                        Para aplicar uma atividade em um computador, basta acessar o site e clicar em 
                        visualizar.
                        Se preferir, você pode baixar a atividade.
                        Você pode salvar o site nos favoritos para agilizar ainda mais a aplicação
                        da aula, caso use-a em um laboratório de informática.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                </div>
                </div>
            </div>
        </div>
        <!-- Modal de ajuda -->
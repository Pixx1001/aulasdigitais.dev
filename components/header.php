<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aulas Digitais</title>
    <link rel="stylesheet" href="css/custom.css">
        <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="lib/bootstrap-4.0.0/css/bootstrap.css">


</head>

<body>
    <!-- Header & Navbar -->
    <div class="headerBg">
        <nav class="navbar navbar-expand-lg navbar-dark  ">
            <a class="navbar-brand" href="index.html"> <span class="fa fa-book"></span> aulasdigitais.online</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <div class="navbar-nav ml-auto ">
                    <a class="nav-item nav-link active" href="index.php"><span class="fa fa-home"> </span> Home<span
                            class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link d-lg-none"
                        href="upload.php"><span
                            class="fa fa-upload"> </span> Envie sua aula</a>
                    <a data-toggle="modal" data-target="#modalAjuda" class="nav-item nav-link " href="#"
                        id="sobre"><span class="fa fa-question"> </span> Ajuda</a>
                    <a data-toggle="modal" data-target="#exampleModal" class="nav-item nav-link " href="#"><span
                            class="fa fa-info"></span> </span> Sobre</a>
                </div>
            </div>
        </nav>
        <div class="container-fluid text-left">
            <h1 class="headStyle">Biblioteca de aulas digitais</h1><a href="upload.php" style="margin-right:3%;" class=" mt-0 btn btn-outline-light pull-right btn-aula d-lg-block "><i class="fa fa-upload"> </i> Enviar aula</a>
            <p class="text-light">Lista de todas as aulas enviadas pelos usuários do site.<span class="btn-aula d-lg-inline" > Se você também quer contribuir, clique em 'Enviar aula'</span> 
            </p>
        </div>
    </div>
    <div style="height: 150px; overflow: hidden;">
            <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 30%; width: 100%;">
              <path d="M0.00,92.27 C216.83,192.92 304.30,8.39 500.00,109.03 L500.00,0.00 L0.00,0.00 Z" style="stroke: none;fill: #0074D9;;"></path>
            </svg>
    </div>
    <!-- Header & Navbar -->
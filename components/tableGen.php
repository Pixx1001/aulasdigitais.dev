<table class="table table-stripped text-center">
<tr>
    <thead>
        <th>Aula</th>
        <th colspan="2">Ação</th>
    </thead>
</tr>
<?php
 
    if ($dir = opendir('aulas')) {
        
        // Varrer diretório
        while (false !== ($file = readdir($dir))) {
            if($file == '.' || $file == '..' || $file == 'zip'){
                continue;
            }

            echo '<tr>';
                echo '<td>'."<a class=\"atividade text-dark\" href=aulas/".rawurlencode("$file")." > $file </a> ".'</td>';
                echo '<td>'."<a class=\"atividade \" href=aulas/".rawurlencode("$file")." ><i class=\"fa fa-eye\" ></i> Visualizar</a> ".'</td>';
                echo '<td>'."<a class=\"atividade \" href=aulas/zip/".rawurlencode("$file").'.zip'." ><i class=\"fa fa-download\" ></i> Baixar</a> ".'</td>';
            echo '</tr>'; 


            
            $filename = $file;
            
            //echo "modificado em: " . date ("F d Y H:i:s.", fileatime("aulas/$filename"));

        }

        closedir($dir);
    }
?> 
</table>
<?php include('components/header.php'); ?>
    <div class="container-fluid mt-3" >
        <div class="row text-left m-auto" style="width:95%;" >
            <div class="col-md-12">
                <h3 class="">Envie sua aula digital</h3>
                    <p>
                        Aqui você pode contribuir com suas aulas digitais que podem ser enviadas por esta página.
                        Pedimos que você leia os critérios antes de enviar sua atividade. Nós aceitamos atividades
                        feitas no Ardora, EdiLIM ou similiares feitos com recursos Web (HTML5,JavaScript, flash, etc). Contribua
                        para a educação digital enviando suas melhores aulas feitas!
                    </p>
            </div>
        </div>

        <br>

        <div class="row m-auto" style="width:95%;">
            <div class="col-md-4 text-left">
                <div class="card">
                    <div class="card-body">
                        <p class="card-text">
                            <b>Critérios para aceitação das atividades: <hr> </b>
                            <b>1</b> - Sua atividade deve rodar em um navegador. </br>
                            <b>2</b> - Sua atividade deve ter no mínimo 10 questões. </br>
                            <b>3</b> - Enviar no formato .zip                        </br>
                            <b>4</b> - Não conter nenhum conteúdo ilegal (pirataria) </br>
                    </div>
                </div>

            </div>

            <div class="col-md-8">
                <form enctype="multipart/form-data" method="post" action="uploadHandler.php" class="card mt-4 m-auto p-3 form-group d-block">
                    <b>Envie sua aula digital:<hr></b>
                    <div class="row w-75 m-auto">
                        <div class="col-sm">
                            <input type="text" name="nome" class="form-control" placeholder="Digite seu nome" required>
                        </div>
                        <div class="col-md">
                            <input type="text" name="email" class="form-control" placeholder="Digite seu email" required>
                        </div>
                    </div>
                    <br>

                    <input class="form-control-file w-50 d-block text-center m-0 m-auto" style="width:100%;" type="file" value="Envia" name="file[]" id="file" multiple="true" required>
                    <br>
                    <button type="submit" id="upload" class="btn btn-primary">
                        <span class="fa fa-upload"></span> Enviar 
                    </button>
                </form>
            </div>

        </div>



        



<?php include('components/modals.php'); ?>
<?php include('components/footer.php'); ?>
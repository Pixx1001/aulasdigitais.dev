//Creado con Ardora - www.webardora.net
//bajo licencia Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
//para otros usos contacte con el autor
var timeAct=360; timeIni=360; timeBon=0;
var successes=0; successesMax=2; attempts=0; attemptsMax=1;
var score=0; scoreMax=2; scoreInc=1; scoreDec=1
var typeGame=0;
var tiTime=false;
var tiTimeType=0;
var tiButtonTime=true;
var textButtonTime="Iniciar";
var tiSuccesses=false;
var tiAttempts=false;
var tiScore=false;
var startTime;
var colorBack="#FFFDFD"; colorButton="#91962F"; colorText="#000000"; colorSele="#FF8000";
var goURLNext=false; goURLRepeat=false;tiAval=false;
var scoOk=0; scoWrong=0; scoOkDo=0; scoWrongDo=0; scoMessage=""; scoPtos=10;
var fMenssage="Verdana, Geneva, sans-serif";
var fActi="Verdana, Geneva, sans-serif";
var fResp="Verdana, Geneva, sans-serif";
var fEnun="Verdana, Geneva, sans-serif";
var timeOnMessage=5; messageOk="MUITO BEM!"; messageTime=""; messageError=""; messageErrorG=""; messageAttempts=""; isShowMessage=false;
var urlOk=""; urlTime=""; urlError=""; urlAttempts="";
var goURLOk="_blank"; goURLTime="_blank"; goURLAttempts="_blank"; goURLError="_blank"; 
borderOk="#008000"; borderTime="#FF0000";borderError="#FF0000"; borderAttempts="#FF0000";
var wordsGame="cGVyZ3VudGFz"; wordsStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
function giveZindex(typeElement){var valueZindex=0; capas=document.getElementsByTagName(typeElement);
for (i=0;i<capas.length;i++){if (parseInt($(capas[i]).css("z-index"),10)>valueZindex){valueZindex=parseInt($(capas[i]).css("z-index"),10);}}return valueZindex;}
var tags=["<h1 style=\"color:blue;\">O que devo fazer se eu estiver com algum problema no computador?</h1><br><h4>Clique no quadradinho ao lado da alternativa para escolher uma alternativa, depois, clique no quadrado com o símbolo [?] para responder</h4>","<h1 style=\"color:blue;\">O que devo fazer quando a aula de informática acabar?</h1><br><h4>Clique no quadradinho ao lado da alternativa para escolher uma alternativa, depois, clique no quadrado com o símbolo [?] para responder</h4>"];
var answers1=["MUxldmFudGFyIG1pbmhhIG3jbyBlIGVzcGVyYXIgbyBtb25pdG9yIGRlIGluZm9ybeF0aWNhIHZpcg","ME7jbyBmYXplciBuYWRh","ME1lIGxldmFudGFyIGRhIG1lc2EgZSBpciBhdOkgbyBtb25pdG9y","MEZpY2FyIGdyaXRhbmRvIG8gbm9tZSBkbyBtb25pdG9yIGRlIGluZm9ybeF0aWNh"];
var answers2=["MU7jbyBkZXNsaWdhciBvIGNvbXB1dGFkb3IsIG9yZ2FuaXphciBhcyBjYWRlaXJhcywgbGV2YXIgbWV1cyBtYXRlcmlhaXMgY2FzbyBldSB0ZW5oYSBsZXZhZG8gYWxndW0gZXNwZXJhciBvIHByb2Zlc3NvciBjaGFtYXI","MFNhaXIgZGEgbWVzYSwgZGVpeGFyIGFzIGNhZGVpcmFzIGRlIHF1YWxxdWVyIGplaXRvIGUgaXIgcHJhIHNhbGE","MERlc2xpZ2FyIG8gY29tcHV0YWRvciBlIGlyIHBhcmEgYSBtaW5oYSBzYWxhIGRlIGF1bGE","MFNhaXIsIG7jbyBmYXplciBuYWRhIG5vIGNvbXB1dGFkb3IgZSBkZWl4YXIgYXMgY2FkZWlyYXMgZGVzb3JnYW5pemFkYXM"];
var ans=[answers1,answers2];
var err=["ERRADO! TENTE NOVAMENTE!","ERRADO! TENTE DE NOVO!"];
var ima=["Man-with-question-mark.jpg","red-big-question-mark-and-toon-man-thinking-faq-ask-search-concepts-michal-bednarek.jpg"];
var mp4=["",""];
var ogv=["",""];
var indexTag=0; actualAnswers=[]; dirMedia="perguntas_resources/media/";
var tiRandOrder=false;
var iT=0;var r_order=[];
colorText="#000000";colorButton="#FFC400";colorBack="#FFFFFF";colorSele="#82B2FD";goURLNext=true;
fMenssage="Verdana, Geneva, sans-serif";fActi="Verdana, Geneva, sans-serif";fEnun="Verdana, Geneva, sans-serif";
tiAval=true;
